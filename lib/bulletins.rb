require "bulletins/version"
require "bulletins/engine"

module Bulletins
  class Error < StandardError; end
  mattr_accessor :distribution_class_name, default: "Bulletins::BasicDistribution"
  mattr_accessor :distribution_controller, default: "Bulletins::BasicDistributionsController"
  mattr_accessor :bulletin_form_partial, default: "basic_distribution_form"
end
