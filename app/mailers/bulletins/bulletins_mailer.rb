module Bulletins
  class BulletinsMailer < ApplicationMailer
    def bulletin
      @bulletin = params[:bulletin]
      mail to: params[:email], subject: @bulletin.subject
    end
  end
end
