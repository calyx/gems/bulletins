module Bulletins
  class BulletinsController < ApplicationController
    before_action :set_bulletin, only: %i[ show edit update destroy ]

    # GET /bulletins
    def index
      @bulletins = Bulletin.all
    end

    # GET /bulletins/1
    def show
    end

    # GET /bulletins/new
    def new
      @bulletin = Bulletin.new
    end

    # GET /bulletins/1/edit
    def edit
    end

    # POST /bulletins
    def create
      @bulletin = Bulletin.new(bulletin_params)

      if @bulletin.save
        redirect_to @bulletin, notice: "Bulletin was successfully created."
      else
        render :new, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /bulletins/1
    def update
      if @bulletin.update(bulletin_params)
        redirect_to @bulletin, notice: "Bulletin was successfully updated."
      else
        render :edit, status: :unprocessable_entity
      end
    end

    # DELETE /bulletins/1
    def destroy
      @bulletin.destroy
      redirect_to bulletins_url, notice: "Bulletin was successfully destroyed."
    end

    # POST /bulletins/1/send_emails
    def send_emails
      @bulletin.send_emails
    end

    private
      def set_bulletin
        @bulletin = Bulletin.find(params[:id])
      end

      def bulletin_params
        params.require(:bulletin).permit(:subject, :content, :distribution_id)
      end
  end
end
