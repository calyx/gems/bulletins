module Bulletins
  class BasicDistributionsController < ApplicationController
    before_action :set_distribution, only: %i[ show edit update destroy add_recipient ]

    def index
      @distributions = BasicDistribution.all
    end

    def show
    end

    def new
      @distribution = BasicDistribution.new
    end

    def edit
    end

    def create
      @distribution = BasicDistribution.new(distribution_params)

      if @distribution.save
        redirect_to @distribution, notice: "Distribution was successfully created."
      else
        render :new, status: :unprocessable_entity
      end
    end

    def add_recipient
      @distribution.add_recipient params[:email]
      redirect_to action: :edit
    end

    def update
      if @distribution.update(distribution_params)
        redirect_to @distribution, notice: "Distribution was successfully updated."
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @distribution.destroy
      redirect_to basic_distributions_url, notice: "Distribution was successfully destroyed."
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_distribution
        @distribution = BasicDistribution.find(params[:id])
      end

      # Only allow a list of trusted parameters through.
      def distribution_params
        params.require(:basic_distribution).permit(:name)
      end
  end
end
