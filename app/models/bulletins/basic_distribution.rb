module Bulletins
  ##
  # The default distribution model.
  #
  # Uses DistributionRecipient and Recipient to store email addresses.
  class BasicDistribution < Distribution
    self.table_name = "bulletins_distributions"
    has_many :distribution_recipients, foreign_key: "distribution_id"
    has_many :recipients, through: :distribution_recipients
    validates :name, presence: true

    # Adds email address to this distribution
    def add_recipient(email)
      self.class.transaction do
        DistributionRecipient.find_or_create_by!(
          distribution: self,
          recipient: Recipient.find_or_create_by!(email: email)
        )
      end
    end

    def name_with_count
      name + " (" + recipients.count.to_s + ")"
    end
  end
end
