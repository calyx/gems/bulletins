module Bulletins
  # A bulletin has one distribution, which is set of email recipients.
  #
  # Subclass or use a guide when creating a custom system for retrieving bulletin recipients.
  class Distribution < ApplicationRecord
    self.abstract_class = true
    has_many :bulletins
  end
end
