module Bulletins
  # Join table that links BasicDistribution and Recipient
  class DistributionRecipient < ApplicationRecord
    belongs_to :distribution, foreign_key: "distribution_id", class_name: "Bulletins::BasicDistribution", inverse_of: :distribution_recipients
    belongs_to :recipient
  end
end
