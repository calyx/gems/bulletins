module Bulletins
  class Bulletin < ApplicationRecord
    has_rich_text :content
    validates :subject, length: { maximum: 191 }
    belongs_to :distribution, class_name: Bulletins.distribution_class_name, optional: true

    # TODO once sent this will be inaccurate if the distribution has changed
    def recipients
      distribution&.recipients
    end

    def draft?
      !sent?
    end

    def sent?
      sent_at.present?
    end

    def readonly
      sent?
    end

    # Send out email to all recipients
    def send_emails
      if sent?
        raise Error, "Bulletin \##{id} has already been sent."
      end
      # BulletinMailerJob.perform_later
      update sent_at: Time.current, sent_count: recipients.count
    end
  end
end
