module Bulletins
  # Table with email addresses
  class Recipient < ApplicationRecord
    has_many :distribution_recipients
    has_many :distributions, through: :distribution_recipients
  end
end
