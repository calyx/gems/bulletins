module Bulletins
  module ApplicationHelper
    def basic_distribution_options_for_select(selected = nil)
      options_for_select(Bulletins::BasicDistribution.all.map do |d|
        [d.name_with_count, d.id]
      end, selected: selected)
    end
  end
end
