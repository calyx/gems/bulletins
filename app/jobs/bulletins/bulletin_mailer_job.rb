module Bulletins
  # This enqueues a BulletinsMailer#bulletin email for each bulletin recipient
  class BulletinMailerJob < ApplicationJob
    queue_as :default

    def perform(bulletin_id)
      bulletin = Bulletin.find(bulletin_id)
      Rails.logger.info "Sending Bulletin\##{bulletin} (#{bulletin.subject}) to #{bulletin.recipients.count} recipients"

      bulletin.recipients.find_each do |recipient|
        BulletinsMailer
          .with(bulletin: bulletin, email: recipient.email)
          .bulletin
          .deliver_later
      end
    end
  end
end
