# Bulletins

Rails plugin for drafting and sending email bulletins

## Usage

## Installation
Add this line to your application's Gemfile:

```ruby
gem "bulletins"
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install bulletins
```
