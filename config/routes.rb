Bulletins::Engine.routes.draw do
  resources :bulletins do
    member do
      post 'send_emails'
    end
  end
  # For BasicDistribution
  # Bulletins.distribution_controller.constantize.controller_name.to_sym
  resources :basic_distributions do
    member do
      post 'add_recipient'
    end
  end
end
