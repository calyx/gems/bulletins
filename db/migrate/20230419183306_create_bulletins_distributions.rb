class CreateBulletinsDistributions < ActiveRecord::Migration[7.0]
  def change
    create_table :bulletins_distributions do |t|
      t.string :name

      t.timestamps
    end
  end
end
