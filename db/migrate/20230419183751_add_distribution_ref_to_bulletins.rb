class AddDistributionRefToBulletins < ActiveRecord::Migration[7.0]
  def change
    add_column :bulletins_bulletins, :distribution_id, :bigint
  end
end
