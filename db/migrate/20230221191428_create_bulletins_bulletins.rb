class CreateBulletinsBulletins < ActiveRecord::Migration[7.0]
  def change
    create_table :bulletins_bulletins do |t|
      t.timestamp :sent_at
      t.integer :sent_count
      t.string :subject

      t.timestamps
    end
  end
end
