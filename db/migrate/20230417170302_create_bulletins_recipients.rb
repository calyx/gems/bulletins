class CreateBulletinsRecipients < ActiveRecord::Migration[7.0]
  def change
    create_table :bulletins_recipients do |t|
      t.text :email, null: false

      t.timestamps
    end
    add_index :bulletins_recipients, :email, unique: true
  end
end
