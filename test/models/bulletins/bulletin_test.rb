require "test_helper"

module Bulletins
  class BulletinTest < ActiveSupport::TestCase
    test 'sent?/draft?' do
      assert Bulletin.new(sent_at: 1.day.ago).sent?
      assert !Bulletin.new.sent?
      assert Bulletin.new.draft?
    end
  end
end
