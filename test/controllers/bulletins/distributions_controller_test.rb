require "test_helper"

module Bulletins
  class DistributionsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @distribution = bulletins_basic_distributions(:one)
    end

    test "should get index" do
      get basic_distributions_url
      assert_response :success
    end

    test "should get new" do
      get new_basic_distribution_url
      assert_response :success
    end

    test "should create distribution" do
      assert_difference("BasicDistribution.count") do
        post basic_distributions_url, params: { basic_distribution: { name: @distribution.name } }
      end

      assert_redirected_to basic_distribution_url(BasicDistribution.last)
    end

    test "should show distribution" do
      get basic_distribution_url(@distribution)
      assert_response :success
    end

    test "should get edit" do
      get edit_basic_distribution_url(@distribution)
      assert_response :success
    end

    test "should update distribution" do
      patch basic_distribution_url(@distribution), params: { basic_distribution: { name: @distribution.name } }
      assert_redirected_to basic_distribution_url(@distribution)
    end

    test "should destroy distribution" do
      assert_difference("BasicDistribution.count", -1) do
        delete basic_distribution_url(@distribution)
      end

      assert_redirected_to basic_distributions_url
    end
  end
end
