module Bulletins
  # Preview all emails at http://localhost:3000/rails/mailers/bulletins_mailer
  class BulletinsMailerPreview < ActionMailer::Preview

    # Preview this email at http://localhost:3000/rails/mailers/bulletins_mailer/bulletin
    def bulletin
      BulletinsMailer.with(
        bulletin: Bulletin.last,
        email: "test@example.com"
      ).bulletin
    end

  end
end
