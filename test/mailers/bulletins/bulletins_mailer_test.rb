require "test_helper"

module Bulletins
  class BulletinsMailerTest < ActionMailer::TestCase
    setup do
      @bulletin = Bulletins::Bulletin.create!(
        subject: "This is a bulletin subject",
        content: "<p>hi</p>"
      )
    end

    test "bulletin" do
      mail = BulletinsMailer
               .with(bulletin: @bulletin,
                     email: "to@example.org")
               .bulletin
      assert_equal "This is a bulletin subject", mail.subject
      assert_equal ["to@example.org"], mail.to
      assert_equal ["from@example.com"], mail.from
      assert_equal "hi", mail.parts.detect { |part| part.mime_type == "text/plain" }.body.to_s.strip
      assert_match "<p>hi</p>", mail.parts.detect {|part| part.mime_type == "text/html"}.body.to_s
    end
  end
end
